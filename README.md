# etch-a-sketch
A Javascript/JQuery Practice Project from The Odin Project's [curriculum](http://www.theodinproject.com/web-development-101/lessons/javascript-and-jquery)

View it [here](https://luckysvenn.github.io/etch-a-sketch/)
