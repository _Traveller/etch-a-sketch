// Entry Point
$(document).on("ready", function() {
    // Our Grid will always start as a 16x16 grid
    let numOfSquares = 16;

    // Build the grid
    buildGrid(numOfSquares);    
    // Set the size of each square
    setSquareSize(numOfSquares);
    // And start drawing as the mouse hovers over each square
    draw(); 
});

// Runs when the user hits the "Clear" button on the page
function reset() {
    let numOfSquares = +prompt("How big would you like the grid now? " +
                               "(Example: 20 => 20x20 Grid)\n**WARNING**" + 
                               ": SIZES LARGER THAN 100 WILL CAUSE SLOWDOWN"); 

    //Empty our container of all its chilren
    $("#grid-container").empty();
    // Reset the color of each square to grey
    $('square').css("background-color", "#BBB");
    // Rebuild our grid with our new specified number
    buildGrid(numOfSquares);
    // And get our new square size for a perfect grid
    setSquareSize(numOfSquares);
    // Draw when the mouse hovers over the squares
    draw(); 
}

// Create our Grid
function buildGrid(num) {
    for(let i = 0; i < num; i++) { 
        for(let j = 0; j < num; j++)
            $("#grid-container").append($('<div class="square"></div>'));
    }
}

// Calculate how big each square should be for a perfect grid
function setSquareSize(num) {
    $(".square").css("width", 640/num);
    $(".square").css("height", 640/num);
}

// When the mouse hovers over a square, change it's color to black
// so it leaves a drawn trail
function draw() {
    $(".square").on("mouseenter", function() {
        $(this).css("background-color", "#000");
    });
}
